![O5uRsK.png](https://s1.ax1x.com/2022/05/17/O5uRsK.png)
### 🔥  介绍

最终效果可以观看视频：https://h5.pipix.com/s/NTHB9gk/
### 💥  项目结构

![image.png](https://img-blog.csdnimg.cn/img_convert/12edeee044490c932337a4b63e453e05.png)
### 🔥  相关技术springboot

使用框架springboot，快速搭建后端服务。  
###🔥  搭建教程
打开天猫精灵技能官网：https://iap.aligenie.com/
![image.png](https://img-blog.csdnimg.cn/img_convert/de0fd99aaa3e3f541b9ba5cb6e03b13b.png)
然后选择私域技能：  
![image.png](https://img-blog.csdnimg.cn/img_convert/193e3134992c23dc5125d81da75119c1.png)
语音交互模型，虽然我们无调用词，因为无调用词，需要获取你的设备openid，后面会介绍到。  
![image.png](https://img-blog.csdnimg.cn/img_convert/ca6e780a06ae7195baefb3715ff679fe.png)
点击创建一个意图。  
![image.png](https://img-blog.csdnimg.cn/img_convert/5bc2b1b17ec958a398b2def2a1d7e516.png)
![image.png](https://img-blog.csdnimg.cn/img_convert/b68a58b1d496845da5222fe7af5ceeba.png)
这里一定要设置默认意图。一会调用就可以获取到我们无调用词的相关数据了。  
服务部署。  
![image.png](https://img-blog.csdnimg.cn/img_convert/2c1231199bb224a6b6c85ac547cc78ca.png)
![image.png](https://img-blog.csdnimg.cn/img_convert/a1d963794a9c0610aba0dc983ac2c9a8.png)
其实这里就是验证这个服务器是你的。  
![image.png](https://img-blog.csdnimg.cn/img_convert/ba9836e0be60c14b1ef693b646ba4db0.png)
下载认证数据。

![image.png](https://img-blog.csdnimg.cn/img_convert/f81cdafd2af38c7eed3477d9b8e3c105.png)
这就是添加好了的。  
如果要使用推送功能必须要申请这个权限。  
![image.png](https://img-blog.csdnimg.cn/img_convert/0254468c6f979b719ff96e1d0ab2d848.png)
下面就是申请推送语音播报的模板了。  
![image.png](https://img-blog.csdnimg.cn/img_convert/1343047752d5feb1a409175b4817dc61.png)
点击新建，可以根据个人需要进行申请。  
![image.png](https://img-blog.csdnimg.cn/img_convert/9c99d36d819e22aae67cee545f358800.png)
![image.png](https://img-blog.csdnimg.cn/img_convert/fbb782e9ee36da3c0fe4f38a9b79d608.png)
下面就是测试了。我们开始搭建服务端。  
其实这个就回到了最初的哪个问题，我们是无调用词的，怎么触发服务端的代码呢？如何获取一些我们需要的东西呢？  
![image.png](https://img-blog.csdnimg.cn/img_convert/91191bc2378f3ee9f4233ee8964f1a70.png)
我们在编辑界面，设置一下调用词  
![image.png](https://img-blog.csdnimg.cn/img_convert/dfbc6b0742292be0266bf5a44363fdd7.png)
然后写好代码，然后上传服务器。
```js
    @RequestMapping("/welcome")
    public ResultModel<TaskResult> taskResult(@RequestBody String json){
        // ResultModel<TaskResult> res = new ResultModel<>();
        log.info("json:{}",json);
        TaskQuery taskQuery =MetaFormat.parseToQuery(json);
        TaskResult taskResult = new TaskResult();
        // 从请求中获取意图参数以及参数值
        Map<String, String> paramMap = taskQuery.getSlotEntities().stream().collect(
                Collectors.toMap(slotItem -> slotItem.getIntentParameterName(),
                        slotItem -> slotItem.getOriginalValue()));
        //处理名称为 welcome 的意图
        if ("welcome".equals(taskQuery.getIntentName())) {
            taskResult.setReply("欢迎使用自定义技能~");
            log.info("json:{}",json);
            //处理名称为 weather 的意图
        }else {
            taskResult.setReply("请检查意图名称是否正确，或者新增的意图没有在代码里添加对应的处理分支。");
        }
        return reply(taskResult);
    }
```
因为我们需要相关id，我们需要在测试界面，进行真机测试  
![image.png](https://img-blog.csdnimg.cn/img_convert/c899f54f012a1455b83faa943c77a893.png)
实际调用，可以看到我们需要的数据，我们可以用到。  
![image.png](https://img-blog.csdnimg.cn/img_convert/2d4ee6e77c772d552785cc5d62ac0f47.png)
### 🔥实现自动推送

打开自动推送:  
https://open-api.aligenie.com/?version=iap_1.0&apiName=PushNotifications  
![image.png](https://img-blog.csdnimg.cn/img_convert/95241493d1d3a840b27906d66dab7c7b.png)
可以看到我们需要这些参数，最重要的是刚才我们在服务端获取的。  
DEVICE_UNION_ID ：设备unionId  
DEVICE_OPEN_ID ：设备openId  
USER_UNION_ID ：用户unionId  
USER_OPEN_ID ：用户openId  
这些值。我们需要用到的。剩下的跟着官方教程就可以完成了。  
最后附上项目代码地址：
https://github.com/CoderXGC/aligenie-demo
https://gitee.com/CoderXGC/aligenie-demo
## 交流
![Image text](https://i.loli.net/2021/11/29/Rm1SX7JWPBEDsat.png)
## 捐助
![Image text](https://www.ylesb.com/wp-content/uploads/2022/04/1651062390-642.png)
